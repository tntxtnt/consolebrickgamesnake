#include "snakegame.h"

void SNAKE_init()
{
    queue_coord_init(&SNAKE_game.snake_body);
    valarray_coord_init(&SNAKE_game.walls);
    SNAKE_game.food.X = SNAKE_game.food.Y = -1;
    SNAKE_reset();
    atexit(__SNAKE_free);
}

void SNAKE_reset()
{
    // Delete old snake
    for (int i = 0; i < SNAKE_game.snake_body.nq.size; ++i)
        BRICK_grid_set(SNAKE_game.snake_body.nq.data[i], BACK_COLOR);
    for (int i = 0; i < SNAKE_game.snake_body.dq.size; ++i)
        BRICK_grid_set(SNAKE_game.snake_body.dq.data[i], BACK_COLOR);
    __SNAKE_free();
    // Delete old walls (BLANK - no walls)
    // Delete old food
    if (SNAKE_game.food.X != -1)
        BRICK_grid_set(SNAKE_game.food, BACK_COLOR);

    BRICK_reset_level();
    BRICK_reset_speed();
    BRICK_reset_score();

    SNAKE_game.snake_color = ALIVE;

    SNAKE_game.snake_direction = SNAKE_game.snake_next_dir = RIGHT_DIR;

    for (int i = 0; i < 3; ++i)
    {
        COORD c = {i, GRID_ROWS-1};
        queue_coord_enqueue(&SNAKE_game.snake_body, c);
        BRICK_grid_set(c, SNAKE_game.snake_color);
    }

    SNAKE_game.food.X = SNAKE_game.food.Y = -1;
    __SNAKE_random_food();

    SNAKE_game.time_between_snake_movement = 1.0;
    __SNAKE_reset_speed();
    SNAKE_game.time_until_next_snake_move = SNAKE_game.time_between_snake_movement;

    SNAKE_game.snake_moving = 0;
}

void __SNAKE_free()
{
    queue_coord_free(&SNAKE_game.snake_body);
    valarray_coord_free(&SNAKE_game.walls);
}

void __SNAKE_gameover()
{
    SNAKE_game.snake_moving = 0;
    SNAKE_game.snake_color = DEAD;
    for (int i = 0; i < SNAKE_game.snake_body.nq.size; ++i)
        BRICK_grid_set(SNAKE_game.snake_body.nq.data[i], SNAKE_game.snake_color);
    for (int i = 0; i < SNAKE_game.snake_body.dq.size; ++i)
        BRICK_grid_set(SNAKE_game.snake_body.dq.data[i], SNAKE_game.snake_color);
    // High score
    if (BRICK_game.hiscore < BRICK_game.score)
        BRICK_game.hiscore = BRICK_game.score;
}

void __SNAKE_move()
{
    if (!SNAKE_game.snake_moving) return;
    SNAKE_game.snake_direction = SNAKE_game.snake_next_dir;
    COORD tail = queue_coord_dequeue(&SNAKE_game.snake_body);
    BRICK_grid_set(tail, BACK_COLOR);
    COORD head = queue_coord_front(&SNAKE_game.snake_body);
    head.X += SNAKE_game.snake_direction.X;
    head.Y += SNAKE_game.snake_direction.Y;
    if (__SNAKE_wall_collision(head)) //dead
    {
        __SNAKE_gameover();
    }
    else
    {
        int collider = __SNAKE_collision(head);
        if (collider == BACK_COLOR) //free cell
        {
            queue_coord_enqueue(&SNAKE_game.snake_body, head);
            BRICK_grid_set(head, SNAKE_game.snake_color);
        }
        else if (collider == FOOD)
        {
            queue_coord_enqueue(&SNAKE_game.snake_body, head);
            BRICK_grid_set(head, SNAKE_game.snake_color);
            valarray_coord_push_back(&SNAKE_game.snake_body.dq, tail);
            BRICK_grid_set(tail, SNAKE_game.snake_color);
            SNAKE_game.food.X = SNAKE_game.food.Y = -1;
            __SNAKE_random_food();
            // score
            BRICK_game.score += 100;
            // speed
            if (BRICK_game.score % 500 == 0) //incr speed every 500 score
            {
                BRICK_increase_speed(TRUE);
                __SNAKE_incr_speed();
                if (BRICK_game.speed == 1)
                    __SNAKE_reset_speed();
            }
        }
        else //hit wall or self
        {
            __SNAKE_gameover();
        }
    }
    SNAKE_game.snake_moving = 0; //move complete
}

int  __SNAKE_wall_collision(COORD c)
{
    return c.X < 0 || c.X >= GRID_COLS || c.Y < 0 || c.Y >= GRID_ROWS;
}

int  __SNAKE_collision(COORD c)
{
    return BRICK_game.grid[c.Y][c.X];
}

void __SNAKE_random_food()
{
    if (SNAKE_game.food.X != -1) return;

    SNAKE_game.food.X = rand() % GRID_COLS;
    SNAKE_game.food.Y = rand() % GRID_ROWS;
    while (BRICK_game.grid[SNAKE_game.food.Y][SNAKE_game.food.X] != BACK_COLOR)
    {
        SNAKE_game.food.X = rand() % GRID_COLS;
        SNAKE_game.food.Y = rand() % GRID_ROWS;
    }
    BRICK_grid_set(SNAKE_game.food, FOOD);
}

void SNAKE_gamelogic()
{
    if (SNAKE_game.snake_color == DEAD) return;
    SNAKE_game.time_until_next_snake_move -= timedelta;
    if (SNAKE_game.time_until_next_snake_move <= 0)
    {
        SNAKE_game.snake_moving = 1;
        while (SNAKE_game.time_until_next_snake_move <= 0)
            SNAKE_game.time_until_next_snake_move += SNAKE_game.time_between_snake_movement;
    }
    __SNAKE_move();
}

int __SNAKE_samedir(COORD dir)
{
    return SNAKE_game.snake_direction.X == dir.X && SNAKE_game.snake_direction.Y == dir.Y;
}

void SNAKE_handleinput(int input)
{
    if (SNAKE_game.snake_color == DEAD) return;
    if (input == 75 && !__SNAKE_samedir(RIGHT_DIR)) //LEFT
    {
        SNAKE_game.snake_next_dir = LEFT_DIR;
        SNAKE_game.snake_moving = 1;
        SNAKE_game.time_until_next_snake_move = SNAKE_game.time_between_snake_movement;
    }
    else if (input == 77 && !__SNAKE_samedir(LEFT_DIR)) //RIGHT
    {
        SNAKE_game.snake_next_dir = RIGHT_DIR;
        SNAKE_game.snake_moving = 1;
        SNAKE_game.time_until_next_snake_move = SNAKE_game.time_between_snake_movement;
    }
    else if (input == 72 && !__SNAKE_samedir(DOWN_DIR)) //UP
    {
        SNAKE_game.snake_next_dir = UP_DIR;
        SNAKE_game.snake_moving = 1;
        SNAKE_game.time_until_next_snake_move = SNAKE_game.time_between_snake_movement;
    }
    else if (input == 80 && !__SNAKE_samedir(UP_DIR)) //DOWN
    {
        SNAKE_game.snake_next_dir = DOWN_DIR;
        SNAKE_game.snake_moving = 1;
        SNAKE_game.time_until_next_snake_move = SNAKE_game.time_between_snake_movement;
    }
}

void __SNAKE_incr_speed()
{
    SNAKE_game.time_between_snake_movement *= 0.82;
}

void __SNAKE_decr_speed()
{
    SNAKE_game.time_between_snake_movement /= 0.82;
}

void __SNAKE_reset_speed()
{
    SNAKE_game.time_between_snake_movement = 1.0;
}
