#ifndef SNAKEGAME_H
#define SNAKEGAME_H

#include "queue_coord.h"
#include "game.h"

extern const int GRID_COLS;
extern const int GRID_ROWS;
extern const COORD    UP_DIR;
extern const COORD  DOWN_DIR;
extern const COORD  LEFT_DIR;
extern const COORD RIGHT_DIR;
extern double timedelta;

enum _snake_color { ALIVE=GREEN, DEAD=RED, FOOD=YELLOW };

struct _SnakeGame {
    queue_coord snake_body;
    COORD snake_direction;
    COORD snake_next_dir; //to further prevent self-collision
    COORD food;
    valarray_coord walls;
    int snake_color;
    double time_between_snake_movement;
    double time_until_next_snake_move;
    int snake_moving; //ensure 1 SNAKE_move() is called per frame
} SNAKE_game;

void SNAKE_init();
void SNAKE_handleinput(int);
void SNAKE_gamelogic();
void SNAKE_reset();


void __SNAKE_free();
void __SNAKE_move();
int  __SNAKE_wall_collision(COORD);
int  __SNAKE_collision(COORD);
void __SNAKE_random_food();
void __SNAKE_gameover();
int  __SNAKE_samedir(COORD);

void __SNAKE_incr_speed();
void __SNAKE_decr_speed();
void __SNAKE_reset_speed();

#endif // SNAKEGAME_H
