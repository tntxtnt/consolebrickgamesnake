#include "fastconrend.h"

void FCR_initwindow(const wchar_t* title, int x, int y,
                    const wchar_t* fontName, int fontSizeX, int fontSizeY,
                    int cursorPresent)
{
    FCR_init();
    FCR_settitle(title);
    FCR_setfont(fontName, fontSizeX, fontSizeY);
    FCR_setsize(x, y);
    FCR_togglecursor(cursorPresent);
}

void FCR_init()
{
    struct _FCR_Window* pWd = &FCR_window;
    pWd->hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if (pWd->hStdOut == INVALID_HANDLE_VALUE)
    {
        perror("Error getting stdout handle\n");
        exit(1);
    }
    pWd->bufCoord.X = pWd->bufCoord.Y = 0;
    pWd->srctWriteRect.Top = 0;
    pWd->srctWriteRect.Left = 0;
    pWd->iw = 0;
    FCR_setcolor(BLACK, WHITE);
    pWd->outBuf = NULL;
}

void FCR_settitle(const WCHAR* title)
{
    SetConsoleTitleW(title);
}

void FCR_setsize(int x, int y)
{
    struct _FCR_Window* pWd = &FCR_window;

    char changeWindowSizeMsg[64];
    sprintf(changeWindowSizeMsg, "mode con: cols=%d lines=%d", x, y);
	system(changeWindowSizeMsg); //// system call /////

    pWd->wdSize.X = x;
    pWd->wdSize.Y = y;
    pWd->srctWriteRect.Bottom = pWd->wdSize.Y - 1;
    pWd->srctWriteRect.Right = pWd->wdSize.X - 1;
    SetConsoleWindowInfo(pWd->hStdOut, TRUE, &pWd->srctWriteRect);

    FCR_cleanup();
    pWd->outBufSize = x * y;
    pWd->outBuf = (CHAR_INFO*)calloc(pWd->outBufSize, sizeof(CHAR_INFO));
    if (!pWd->outBuf)
    {
        perror("Error allocating output buffer\n");
        exit(1);
    }
    FCR_setcolor(BLACK, WHITE);
    for (int i = pWd->outBufSize; i-->0; )
        pWd->outBuf[i].Attributes = pWd->color;
    atexit(FCR_cleanup);
}

void FCR_cleanup()
{
    free(FCR_window.outBuf);
}

void FCR_gotoxy(int x, int y)
{
    FCR_window.iw = x + y*FCR_window.wdSize.X;
    FCR_window.iw %= FCR_window.outBufSize; //prevent overflow
}

void FCR_clrscr()
{
    for (int i = FCR_window.outBufSize; i-->0; )
    {
        FCR_window.outBuf[i].Attributes = FCR_window.color;
        FCR_window.outBuf[i].Char.AsciiChar = ' ';
    }
    FCR_gotoxy(0, 0);
}

int FCR_textcolor()
{
    return FCR_window.color & 0xF;
}

int FCR_backcolor()
{
    return (FCR_window.color>>4) & 0xF;
}

void FCR_setcolor(int textColor, int backColor)
{
    FCR_window.color = (backColor<<4) + textColor;
}

void FCR_settextcolor(int textColor)
{
    FCR_setcolor(textColor, FCR_backcolor());
}

void FCR_setbackcolor(int backColor)
{
    FCR_setcolor(FCR_textcolor(), backColor);
}

void FCR_togglecursor(int flag)
{
    CONSOLE_CURSOR_INFO cursorInfo;
    GetConsoleCursorInfo(FCR_window.hStdOut, &cursorInfo);
    cursorInfo.bVisible = flag; // set the cursor visibility
    SetConsoleCursorInfo(FCR_window.hStdOut, &cursorInfo);
}

void FCR_printf(const char* fmt, ...)
{
    static char buff[1024];
    va_list args;
    va_start(args, fmt);
    vsprintf(buff, fmt, args);
    va_end(args);

    // Write buff to outBuf
    struct _FCR_Window* pWd = &FCR_window;
    int wdX = FCR_window.wdSize.X;
    for (char* ptr = buff; *ptr; ++ptr)
    {
        if (*ptr == '\b')
            --pWd->iw;
        else if (*ptr == '\r')
            pWd->iw = pWd->iw / wdX * wdX;
        else if (*ptr == '\n')
            pWd->iw = (pWd->iw / wdX + 1) * wdX;
        else
        {
            pWd->outBuf[pWd->iw].Char.AsciiChar = *ptr;
            pWd->outBuf[pWd->iw].Attributes = pWd->color;
            ++pWd->iw;
        }
        pWd->iw %= pWd->outBufSize; //prevent overflow
    }
}

void FCR_wprintf(const wchar_t* fmt, ...)
{
    static wchar_t buff[1024];
    va_list args;
    va_start(args, fmt);
    vswprintf_s(buff, 1024, fmt, args);
    va_end(args);

    // Write buff to outBuf
    struct _FCR_Window* pWd = &FCR_window;
    int wdX = FCR_window.wdSize.X;
    for (wchar_t* ptr = buff; *ptr; ++ptr)
    {
        if (*ptr == L'\b')
            --pWd->iw;
        else if (*ptr == L'\r')
            pWd->iw = pWd->iw / wdX * wdX;
        else if (*ptr == L'\n')
            pWd->iw = (pWd->iw / wdX + 1) * wdX;
        else
        {
            pWd->outBuf[pWd->iw].Char.UnicodeChar = *ptr;
            pWd->outBuf[pWd->iw].Attributes = pWd->color;
            ++pWd->iw;
        }
        pWd->iw %= pWd->outBufSize; //prevent overflow
    }
}

void FCR_writebuffer()
{
    WriteConsoleOutputW(
        FCR_window.hStdOut,  // screen buffer to write to
        FCR_window.outBuf,   // buffer to copy from
        FCR_window.wdSize,   // col-row size of chiBuffer
        FCR_window.bufCoord, // top left src cell in chiBuffer
        &FCR_window.srctWriteRect);
}

void FCR_setfont(const wchar_t* fontName, int fontSizeX, int fontSizeY)
{
    CONSOLE_FONT_INFOEX cfi;
    cfi.cbSize = sizeof cfi;
    cfi.nFont = 0;
    cfi.dwFontSize.X = fontSizeX;
    cfi.dwFontSize.Y = fontSizeY;
    cfi.FontFamily = FF_DONTCARE;
    cfi.FontWeight = FW_NORMAL;
    wcscpy(cfi.FaceName, fontName);
    if (!SetCurrentConsoleFontEx(FCR_window.hStdOut, FALSE, &cfi))
    {
        perror("Cannot set font\n");
        exit(1);
    }
}

void FCR_maximize()
{
	CONSOLE_SCREEN_BUFFER_INFO info;
	GetConsoleScreenBufferInfo(FCR_window.hStdOut, &info);
	SMALL_RECT rc;
	rc.Left = rc.Top = 0;
	rc.Right = min(info.dwMaximumWindowSize.X, info.dwSize.X) - 1;
	rc.Bottom = min(info.dwMaximumWindowSize.Y, info.dwSize.Y) - 1;
	SetConsoleWindowInfo(FCR_window.hStdOut, TRUE, &rc);
}
