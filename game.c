#include "game.h"


void BRICK_init()
{
    BRICK_game.grid = (int**)calloc(GRID_ROWS, sizeof(int*));
    for (int i = 0; i < GRID_ROWS; ++i)
        BRICK_game.grid[i] = (int*)calloc(GRID_COLS, sizeof(int));
    atexit(__BRICK_free_grid);

    for (int i = 0; i < GRID_ROWS; ++i)
        for (int j = 0; j < GRID_COLS; ++j)
            BRICK_game.grid[i][j] = BACK_COLOR;

    for (int i = 0; i < 4; ++i)
        for (int j = 0; j < 4; ++j)
            BRICK_game.next[i][j] = BACK_COLOR;

    BRICK_game.score = 0;
    BRICK_game.hiscore = 0;
    BRICK_game.level = 1;
    BRICK_game.speed = 1;
}

void __BRICK_free_grid()
{
    for (int i = 0; i < GRID_ROWS; ++i)
        free(BRICK_game.grid[i]);
    free(BRICK_game.grid);
}

void BRICK_draw_all()
{
    __BRICK_draw_grid(BRICK_game.grid);
    FCR_setcolor(BLACK, WHITE);
    __BRICK_draw_score("SCORE", BRICK_game.score, "%d", 0);
    __BRICK_draw_score("HIGH", BRICK_game.hiscore, "%d", 13);
    __BRICK_draw_next(26);
    __BRICK_draw_score("LEVEL", BRICK_game.level, "%02d", 52);
    __BRICK_draw_score("SPEED", BRICK_game.speed, "%02d", 65);
}

void BRICK_draw_fast()
{
    __BRICK_draw_grid_fast(BRICK_game.grid);
    FCR_setcolor(BLACK, WHITE);
    __BRICK_draw_score_fast(BRICK_game.score, "%d", 0);
    __BRICK_draw_score_fast(BRICK_game.hiscore, "%d", 13);
    __BRICK_draw_next_fast(26);
    __BRICK_draw_score_fast(BRICK_game.level, "%02d", 52);
    __BRICK_draw_score_fast(BRICK_game.speed, "%02d", 65);
}

void __BRICK_draw_cell(int r, int c, int color)
{
    FCR_gotoxy(7*c, 4*r);
    FCR_setbackcolor(BACK_COLOR);
    FCR_wprintf(L"┼──────┼");
    for (int i = 0; i < 3; ++i)
    {
        FCR_gotoxy(7*c, 4*r + i + 1);
        FCR_setbackcolor(BACK_COLOR);
        FCR_wprintf(L"│");
        FCR_setbackcolor(color);
        FCR_wprintf(L"      ");
        FCR_setbackcolor(BACK_COLOR);
        FCR_wprintf(L"│");
    }
    FCR_gotoxy(7*c, 4*r + 4);
    FCR_setbackcolor(BACK_COLOR);
    FCR_wprintf(L"┼──────┼");
}

void __BRICK_draw_cell_fast(int r, int c, int color)
{
    for (int i = 0; i < 3; ++i)
    {
        FCR_gotoxy(7*c + 1, 4*r + i + 1);
        FCR_setbackcolor(color);
        FCR_wprintf(L"      ");
        FCR_setbackcolor(BACK_COLOR);
    }
}

void __BRICK_draw_grid()
{
    for (int y = 0; y < GRID_ROWS; ++y)
        for (int x = 0; x < GRID_COLS; ++x)
            __BRICK_draw_cell(y, x, BRICK_game.grid[y][x]);
}

void __BRICK_draw_grid_fast()
{
    for (int y = 0; y < GRID_ROWS; ++y)
        for (int x = 0; x < GRID_COLS; ++x)
            __BRICK_draw_cell_fast(y, x, BRICK_game.grid[y][x]);
}

void draw_rjust(const char* s, int y_off, int char_width, int right_off)
{
    int x_off = GRID_COLS*7;
    x_off += LEFT_BLANK_WIDTH - right_off - strlen(s)*char_width;
    for (const char* p = s; *p; ++p, x_off += char_width)
        __BRICK_draw_char(*p, x_off, y_off);
}

void __BRICK_draw_score(const char* label, int score, const char* int_fmt, int y_off)
{
    static char s[20];
    draw_rjust(label, y_off, 6, 5);
    sprintf(s, "      ");
    draw_rjust(s, y_off+5, 5, 5);
    sprintf(s, int_fmt, score);
    draw_rjust(s, y_off+5, 5, 5);
}

void __BRICK_draw_score_fast(int score, const char* int_fmt, int y_off)
{
    static char s[20];
    sprintf(s, "      ");
    draw_rjust(s, y_off+5, 5, 5);
    sprintf(s, int_fmt, score);
    draw_rjust(s, y_off+5, 5, 5);
}

void __BRICK_draw_next(int y_off)
{
    draw_rjust("NEXT", y_off, 6, 5);
    for (int y = 0; y < 4; ++y)
        for (int x = 0; x < 4; ++x)
            __BRICK_draw_cell(y+8, x+GRID_COLS+1, BRICK_game.next[y][x]);
}

void __BRICK_draw_next_fast(int y_off)
{
    for (int y = 0; y < 4; ++y)
        for (int x = 0; x < 4; ++x)
            __BRICK_draw_cell_fast(y+8, x+11, BRICK_game.next[y][x]);
}

void BRICK_grid_set(COORD c, int color)
{
    BRICK_game.grid[c.Y][c.X] = color;
}

void BRICK_increase_level(int loop)
{
    BRICK_game.level++;
    if (BRICK_game.level > MAX_LEVEL)
        BRICK_game.level = loop ? 1 : MAX_LEVEL;
}

void BRICK_decrease_level(int loop)
{
    BRICK_game.level--;
    if (BRICK_game.level < 1)
        BRICK_game.level = loop ? MAX_LEVEL : 1;
}

void BRICK_reset_level()
{
    BRICK_game.level = 1;
}

void BRICK_increase_speed(int loop)
{
    BRICK_game.speed++;
    if (BRICK_game.speed > MAX_SPEED)
        BRICK_game.speed = loop ? 1 : MAX_SPEED;
}

void BRICK_decrease_speed(int loop)
{
    BRICK_game.speed--;
    if (BRICK_game.speed < 1)
        BRICK_game.speed = loop ? MAX_SPEED : 1;
}

void BRICK_reset_speed()
{
    BRICK_game.speed = 1;
}

void BRICK_reset_score()
{
    BRICK_game.score = 0;
}

void __BRICK_draw_char(char c, int x_off, int y_off)
{
    switch (c)
    {
    case '0':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┌───┐");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"└───┘");
        break;
    case '1':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L" ─┐  ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  │  ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  │  ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  │  ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L" ─┴─ ");
        break;
    case '2':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"────┐");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┌───┘");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"└────");
        break;
    case '3':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"────┐");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"────┤");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"────┘");
        break;
    case '4':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┐   ┐");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"└───┤");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    ┘");
        break;
    case '5':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┌────");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"└───┐");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"────┘");
        break;
    case '6':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┌────");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"├───┐");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"└───┘");
        break;
    case '7':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┌───┐");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    ┘");
        break;
    case '8':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┌───┐");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"├───┤");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"└───┘");
        break;
    case '9':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┌───┐");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"└───┤");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"────┘");
        break;
    case 'C':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╭───╮");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╰───╯");
        break;
    case 'D':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┌──╮");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   \\");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   /");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"└──╯");
        break;
    case 'E':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╭────");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"├────");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╰────");
        break;
    case 'G':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╭───╮");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│  ─┬");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╰───╯");
        break;
    case 'H':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┬   ┬");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"├───┤");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┴   ┴");
        break;
    case 'I':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L" ─┬─ ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  │  ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  │  ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  │  ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L" ─┴─ ");
        break;
    case 'L':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┬    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╰────");
        break;
    case 'N':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┬   ┬");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│╲  │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│ ╲ │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│  ╲│");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┴   ┴");
        break;
    case 'O':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╭───╮");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╰───╯");
        break;
    case 'P':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╭───╮");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"├───╯");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        break;
    case 'R':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╭───╮");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"├┬──╯");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│ ╲ ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│  ╲");
        break;
    case 'S':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╭───╮");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│    ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╰───╮");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"    │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╰───╯");
        break;
    case 'T':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"──┬──");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  │  ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  │  ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  │  ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  ┴  ");
        break;
    case 'V':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┬   ┬");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"│   │");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╲   ╱");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L" ╲ ╱");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  V  ");
        break;
    case 'X':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"┐   ┌");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L" ╲ ╱ ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"  ╳  ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L" ╱ ╲ ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"╯   ╰");
        break;
    case ' ':
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"     ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"     ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"     ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"     ");
        FCR_gotoxy(x_off, y_off++); FCR_wprintf(L"     ");
        break;
    default:
        break;
    }
}
