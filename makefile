# EXECUTABLES - DO NOT MODIFIED
CXX = gcc
RM = rm -rf
MKDIR = mkdir -p

# HELPFUL COMMANDS
print-%  : ; @echo $* = $($*)

# PROJECT NAME - will be output executable name
PROJECT = console_snake


# Include
INC_DIRS = 

# Linker
LIB_DIRS = 


# BUILD TARGETS - change your compile flags here
BUILD_RELEASE = Release
CFLAGS_RELEASE = -O2
LFLAGS_RELEASE = -s

# Add another build target here

# Switch your build target here
BUILD = $(BUILD_RELEASE)
CFLAGS = -Wall $(CFLAGS_RELEASE)
LFLAGS = $(LFLAGS_RELEASE)


# Input directory
SRCDIR = .

# Modules
MODULES = .

# Output directories - DO NOT MODIFIED
OBJDIR = build/$(BUILD)/obj
EXEDIR = build/$(BUILD)/bin
DEPDIR = build/dependencies
EXE = $(EXEDIR)/$(PROJECT)
EXEWIN = build\$(BUILD)\bin\$(PROJECT).exe

$(EXEDIR):
	$(MKDIR) $(EXEDIR)

TEMPALL_OBJS_DIRS += $(foreach module, $(MODULES), $(OBJDIR)/$(module))
ALL_OBJS_DIRS = $(TEMPALL_OBJS_DIRS:%.=%)
$(ALL_OBJS_DIRS):
	$(MKDIR) $@

TEMP_ALL_DEPS_DIRS +=  $(foreach module, $(MODULES), $(DEPDIR)/$(module))
ALL_DEPS_DIRS = $(TEMP_ALL_DEPS_DIRS:%.=%)
$(ALL_DEPS_DIRS):
	$(MKDIR) $@

# .c, .h, .o, .d
RSRCS = $(foreach module, $(MODULES), $(wildcard $(module)/*.c))
RSRCS_NODOT = $(RSRCS:./%=%)
RDEPS = $(RSRCS_NODOT:%.c=$(DEPDIR)/%.d)
ROBJS = $(RSRCS_NODOT:%.c=$(OBJDIR)/%.o)

$(DEPDIR)/%.d: $(SRCDIR)/%.c | $(ALL_DEPS_DIRS)
	$(CXX) $< -MM -MT "$(<:%.c=$(OBJDIR)/%.o)" > $@
	$(CXX) $< -MM -MT $@ >> $@

-include $(RDEPS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(ALL_OBJS_DIRS)
	$(CXX) -c $(CFLAGS) $< -o $@

	
build: $(ROBJS) | $(EXEDIR)
	$(CXX) $(ROBJS)  $(LFLAGS) -o $(EXE)
	
run:
	start cmd /k "echo %time%  && $(EXEWIN) && echo. && pause && exit"

clean:
	$(RM) build/

.PHONY: run build clean