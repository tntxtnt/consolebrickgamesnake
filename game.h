#ifndef GAME_H
#define GAME_H

#include "fastconrend.h"

extern const int BACK_COLOR;
extern const int GRID_ROWS;
extern const int GRID_COLS;
extern const int LEFT_BLANK_WIDTH;
extern const int MAX_LEVEL;
extern const int MAX_SPEED;
extern double timescale;

struct _BRICK_Game {
    int** grid;
    int next[4][4];
    int score;
    int hiscore;
    int level;
    int speed;
} BRICK_game;

void BRICK_init();
void BRICK_grid_set(COORD, int);
void BRICK_draw_all();
void BRICK_draw_fast();
void BRICK_increase_level(int);
void BRICK_decrease_level(int);
void BRICK_reset_level();
void BRICK_increase_speed(int);
void BRICK_decrease_speed(int);
void BRICK_reset_speed();
void BRICK_reset_score();


void __BRICK_free_grid();
void __BRICK_draw_cell(int, int, int);
void __BRICK_draw_cell_fast(int, int, int);
void __BRICK_draw_grid();
void __BRICK_draw_grid_fast();

void __BRICK_draw_char(char, int, int);
void __BRICK_draw_score(const char*, int, const char*, int);
void __BRICK_draw_score_fast(int, const char*, int);
void __BRICK_draw_next(int);
void __BRICK_draw_next_fast(int);


#endif // GAME_H
