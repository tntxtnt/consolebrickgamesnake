#include "queue_coord.h"

void queue_coord_init(queue_coord* this)
{
    valarray_coord_init(&this->nq);
    valarray_coord_init(&this->dq);
    this->size = 0;
}

void queue_coord_enqueue(queue_coord* this, COORD n)
{
    valarray_coord_push_back(&this->nq, n);
    this->size++;
}

COORD queue_coord_dequeue(queue_coord* this)
{
    if (this->dq.size < 1)
        while (this->nq.size > 0)
            valarray_coord_push_back(&this->dq, valarray_coord_pop_back(&this->nq));
    this->size--;
    return valarray_coord_pop_back(&this->dq);
}

COORD queue_coord_front(const queue_coord* this)
{
    if (this->nq.size > 0)
        return this->nq.data[this->nq.size - 1];
    else
        return this->dq.data[0];
}

void queue_coord_free(queue_coord* this)
{
    valarray_coord_free(&this->nq);
    valarray_coord_free(&this->dq);
    this->size = 0;
}
