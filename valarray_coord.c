#include "valarray_coord.h"

void valarray_coord_init(valarray_coord* this)
{
    this->data = NULL;
    this->size = 0;
    this->cap = 0;
}

void valarray_coord_reserve(valarray_coord* this, int new_cap)
{
    COORD* new_data = (COORD*)malloc(new_cap * sizeof(COORD));
    memcpy(new_data, this->data, this->size * sizeof(COORD));
    free(this->data);
    this->data = new_data;
    this->cap = new_cap;
}

void valarray_coord_push_back(valarray_coord* this, COORD n)
{
    if (this->size == this->cap)
        valarray_coord_reserve(this, this->cap*2 ? this->cap*2 : 1);
    this->data[(this->size)++] = n;
}

COORD valarray_coord_pop_back(valarray_coord* this)
{
    return this->data[--(this->size)];
}

void valarray_coord_free(valarray_coord* this)
{
    free(this->data);
    valarray_coord_init(this);
}
