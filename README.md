## Console Snake Game
#### Summary
A brick game simulation in Windows console. Use "double buffers" for fast rendering.

![Screenshot 01](/md/ss01.png)