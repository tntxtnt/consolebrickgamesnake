#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "highrestimer.h"
#include <time.h>
#include <math.h>
#include "snakegame.h"

#define GRID_COLS_LITERAL 10
#define GRID_ROWS_LITERAL 20
#define LEFT_BLANK_WIDTH_LITERAL 42

const int GRID_COLS = GRID_COLS_LITERAL;
const int GRID_ROWS = GRID_ROWS_LITERAL;
const int LEFT_BLANK_WIDTH = LEFT_BLANK_WIDTH_LITERAL;
const int BACK_COLOR = WHITE;
const int WINDOW_COLS = GRID_COLS_LITERAL*7 + LEFT_BLANK_WIDTH_LITERAL;
const int WINDOW_ROWS = GRID_ROWS_LITERAL*4 + 3;

const int MAX_LEVEL = 15;
const int MAX_SPEED = 15;

const COORD    UP_DIR = { 0, -1};
const COORD  DOWN_DIR = { 0,  1};
const COORD  LEFT_DIR = {-1,  0};
const COORD RIGHT_DIR = { 1,  0};

const double TIME_PER_FRAME = 1.0 / 32;
double timedelta = 0;
double timescale = 1.0;

int main()
{
    srand(time(NULL));

    FCR_initwindow(L"Rắn Săn Mồi", WINDOW_COLS, WINDOW_ROWS, L"Consolas", 4, 6, FALSE);
    BRICK_init();
    SNAKE_init();

    high_res_timer frame_clock;
    init_timer(&frame_clock);
    double sleep_time = 0.0;

    double saved_timescale = timescale;

    int input;
    int quit = 0;
    start_timer(&frame_clock);
    while (!quit)
    {
        // Time delta
        timedelta = get_elapsed_time(&frame_clock) * timescale;
        start_timer(&frame_clock);

        // Handle events/inputs (only one input in a frame...)
        if (_kbhit())
        {
            input = _getch();
            if (input == VK_ESCAPE)
                quit = 1;
            else if (input == VK_SPACE) //pause / unpause
            {
                if (timescale > 0.0)
                {
                    saved_timescale = timescale;
                    timescale = 0;
                }
                else timescale = saved_timescale;
            }

            if (timescale == 0.0)
            {
                if (input == 0xE0) input = _getch();
            }
            else
            {
                if (input == VK_RETURN)
                {
                    SNAKE_reset();
                }
                else if (input == 0xE0) //function key or arrow key
                {
                    input = _getch(); //72-up, 80-down, 75-left, 77-right
                    SNAKE_handleinput(input);
                }
            }
        }

        // Game logic
        SNAKE_gamelogic();

        // Rendering
        FCR_setcolor(BLACK, BACK_COLOR);
        FCR_clrscr();
        BRICK_draw_all();

        // DEBUG - draw sleep_time of last frame
        FCR_gotoxy(0, WINDOW_ROWS - 1);
        FCR_setbackcolor(sleep_time > 0 ? GREEN : RED);
        FCR_wprintf(L"%.2lf", sleep_time*1000);

        FCR_writebuffer();

        // Frame rate controller
        sleep_time = TIME_PER_FRAME - get_elapsed_time(&frame_clock);
        if (sleep_time > 0)
            Sleep((DWORD)(sleep_time*1000));
    }

    return EXIT_SUCCESS;
}
