#include "highrestimer.h"

void init_timer(high_res_timer* this)
{
    this->startTicks.QuadPart = 0;
    QueryPerformanceFrequency(&this->frequency);
}

void start_timer(high_res_timer* this)
{
    QueryPerformanceCounter(&this->startTicks);
}

double get_elapsed_time(high_res_timer* this)
{
    LARGE_INTEGER stopTicks;
    QueryPerformanceCounter(&stopTicks);
    return (double)(stopTicks.QuadPart - this->startTicks.QuadPart)
        / this->frequency.QuadPart;
}
